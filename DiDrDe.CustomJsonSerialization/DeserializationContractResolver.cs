﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace DiDrDe.CustomJsonSerialization
{
    public class DeserializationContractResolver
        : DefaultContractResolver
    {
        public DeserializationContractResolver()
        {
            NamingStrategy = new CamelCaseNamingStrategy();
        }

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var jsonProperties = base.CreateProperties(type, memberSerialization);

            //TODO filter by value if possible??
            foreach (var jsonProperty in jsonProperties)
            {
                jsonProperty.Converter = new DecryptionJsonConverter();
            }

            return jsonProperties;

        }
    }
}
