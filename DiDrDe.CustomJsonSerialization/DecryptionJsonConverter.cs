﻿using Newtonsoft.Json;
using System;

namespace DiDrDe.CustomJsonSerialization
{
    public class DecryptionJsonConverter
        : JsonConverter
    {
        private const string Suffix = "+++++";

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var path = reader.Path;
            var value = (string)reader.Value;
            var valueWithoutSuffix = value.Remove(value.IndexOf(Suffix, StringComparison.Ordinal), Suffix.Length);
            if (objectType == typeof(int))
            {
                var result = Convert.ToInt32(valueWithoutSuffix);
                return result;
            }

            if (objectType == typeof(DateTime))
            {
                var result = Convert.ToDateTime(valueWithoutSuffix);
                return result;
            }
            return value;
        }

        public override bool CanWrite => false;

        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
}
