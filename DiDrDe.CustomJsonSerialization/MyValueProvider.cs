﻿using Newtonsoft.Json.Serialization;
using System;
using System.Reflection;

namespace DiDrDe.CustomJsonSerialization
{
    public class MyValueProvider
        : IValueProvider
    {
        private readonly IValueProvider _valueProvider;
        private readonly PropertyInfo _propertyInfo;

        public MyValueProvider(
            IValueProvider valueProvider, 
            PropertyInfo propertyInfo)
        {
            _valueProvider = valueProvider;
            _propertyInfo = propertyInfo;
        }

        public void SetValue(object target, object value)
        {
            _valueProvider.SetValue(target, value);
            Console.WriteLine($"Value set: {value}");
        }

        public object GetValue(object target)
        {
            var value = _valueProvider.GetValue(target);
            var valueAsString = value.ToString();
            return valueAsString;
        }
    }
}
