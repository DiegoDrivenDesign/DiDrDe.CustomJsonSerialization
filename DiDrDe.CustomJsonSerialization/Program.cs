﻿using DiDrDe.CustomJsonSerialization.Messages.Original;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace DiDrDe.CustomJsonSerialization
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var text = "This is text";
            var number = 1;
            var createdOn = new DateTime(2019, 1, 7, 0, 0, 0, DateTimeKind.Utc);
            //var anotherText = "This is another text";
            //var anotherNumber = 2;
            //var settableValue = "This is a value set with setter";
            //var sampleInner =
            //    new SampleInner(anotherText, anotherNumber)
            //    {
            //        SettableValue = settableValue
            //    };
            //var sample = new SampleMessage(text, number, sampleInner);
            var simple = new SimpleMessage(text, number, createdOn);

            var serializationContractResolver = new SerializationContractResolver();
            var jsonSettings = GetJsonSettings(serializationContractResolver);

            Console.WriteLine("Serializing..");
            
            var json = JsonConvert.SerializeObject(simple, jsonSettings);
            Console.WriteLine(json);

            Console.WriteLine("Deserializing..");
            var deserializationContractResolver = new DeserializationContractResolver();
            var deserializationJsonSettings = GetJsonSettings(deserializationContractResolver);
            var sampleDeserialized = JsonConvert.DeserializeObject<Messages.New.SimpleMessage>(json, deserializationJsonSettings);
            Console.WriteLine(sampleDeserialized);

            Console.ReadLine();
        }

        private static JsonSerializerSettings GetJsonSettings(IContractResolver contractResolver)
        {
            var jsonSettings =
                new JsonSerializerSettings
                {
                    ContractResolver = contractResolver,
                    //Converters = { new EncryptionJsonConverter() }
                };
            return jsonSettings;
        }
    }
}
