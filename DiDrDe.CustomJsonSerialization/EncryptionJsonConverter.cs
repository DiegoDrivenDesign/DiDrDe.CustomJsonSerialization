﻿using Newtonsoft.Json;
using System;

namespace DiDrDe.CustomJsonSerialization
{
    public class EncryptionJsonConverter
        : JsonConverter
    {
        private const string Suffix = "+++++";
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var asText = value.ToString();
            var finalValue = $"{asText}{Suffix}";
            writer.WriteValue(finalValue);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            //var path = reader.Path;
            //var value = (string)reader.Value;
            //var valueWithoutSuffix = value.Remove(value.IndexOf(Suffix, StringComparison.Ordinal), Suffix.Length);
            //if (objectType == typeof(int))
            //{
            //    var result = Convert.ToInt32(value);
            //    return result;
            //}
            //return value;
            throw new NotImplementedException();
        }

        public override bool CanWrite => true;

        public override bool CanRead => false;

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
}
