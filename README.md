# DiDrDe.CustomJsonSerialization

This project demonstrates how to tweak newtonsoft serialization and deserialization with a contract resolver and value provider to set and get values in a custom way (e.g: encryption/decryption of certain fields)